package samira.radfar.widgetmania;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RadioGroup rdoGP;
    Button btn;
    TextView tv;
    CheckBox chkTrans;
    CheckBox chkTint;
    CheckBox chkResize;
    ImageView imv;
    WebView wv;
    int checkedno;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rdoGP = (RadioGroup) findViewById(R.id.RG);
        rdoGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkedno=checkedId;
            }
        });

        btn = (Button) findViewById(R.id.btn);
        tv = (TextView) findViewById(R.id.tv);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowText();
            }
        });

        chkTrans = (CheckBox) findViewById(R.id.chkTrans);
        chkTrans.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true)
                    btn.setAlpha(0.5f);
            }
        });

/*        chkTint = (CheckBox) findViewById(R.id.chkTint);
        chkTint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked == true)

            }
        });*/

/*        chkResize = (CheckBox) findViewById(R.id.chkResize);
        imv = (ImageView) findViewById(R.id.iv);
        chkTint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true)
                imv.setBackground();
                imv.se
            }
        });*/

/*        wv = (WebView) findViewById(R.id.wv);
        wv.loadUrl("https://www.google.com/");*/

    }

    private void ShowText() {
        switch (checkedno) {
            case 1:
                tv.setText("Hello London people");
                break;
            case 2:
                tv.setText("Hello Beijin people");
                break;
            case 3:
                tv.setText("Hello New York people");
                break;
        }

    }
}
